// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import axios from 'axios'


Vue.config.productionTip = false

Vue.use(Antd);

Vue.prototype.$axios = axios;


// Vue.prototype.$baseUrl = 'http://open.lingantech.com:10006';
// Vue.prototype.$baseUrl2 = 'http://open.lingantech.com:10007';
// Vue.prototype.$staticDomainURL = Vue.prototype.$baseUrl+'/filemanage/sys/common/static/files/';

//本地测试
Vue.prototype.$baseUrl = 'http://192.168.2.184:8088';
Vue.prototype.$baseUrl2 = 'http://192.168.2.184:8012';
Vue.prototype.$staticDomainURL = Vue.prototype.$baseUrl+'/filemanage/sys/common/static/files/';

// axios.defaults.baseURL = 'http://192.168.2.91:8081/trainal'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
