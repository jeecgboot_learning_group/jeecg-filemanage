import { getAction, deleteAction, putAction, postAction, httpAction } from '@/api/manage'

//根据设备id查询相关文件
const fileDetailsInfoGetByPid = (params)=>getAction("/filemanage/fmfileDetailsInfo/getByPid",params);

//导入文件列表
const getFileDetailsInfoList = (params)=>getAction("/base/fmFileDetailsInfo/listNopid",params);
const getFileDetailsList = (params)=>getAction("/base/fmFileDetailsInfo/list",params);



//资料需求单管理
const dtgDemandInfoAdd = (params)=>getAction("/base/fmDemandInfo/add",params);

//文件挂接 添加目录
const dtgTypeDetailsAdd = (params)=>postAction("/base/fmTypeDetails/add",params);
const dtgTypeDetailsEdit = (params)=>putAction("/base/fmTypeDetails/edit",params);
const getFileDetailsInfoByPid = (params)=>putAction("/base/fmfileDetailsInfo/getByPid",params);
const setFileDetailsPid =  (params)=>getAction("/base/fmFileDetailsInfo/setPid",params);


//收藏
const addFileDrcInfo = (params)=>postAction("/base/fmFileDrcInfo/addall",params);
const isFileDrcDetails = (params)=>getAction("/base/fmFileDrcDetails/getByPid",params);
const dtgEquipmentInfoEdit = (params)=>putAction("/base/dtgEquipmentInfo/edit",params);


//上传文件
const fileTransManageAdd = (params)=>postAction("/filemanage/fileTransManage/add",params);
const fileTransManageEdit = (params)=>postAction("/filemanage/fileTransManage/edit",params);
const fileTransManageDelete = (params)=>deleteAction("/filemanage/fileTransManage/delete",params);

//导出
const unzipExportzip = (params)=>putAction("/base/Unzip/exportzip",params);

//资料检索
const fileDetailsInfoList = (params)=>getAction("/base/fmfileDetailsInfo/list",params);

//评论
const commentAdd = (params)=>postAction("/base/fmCommentInfo/add",params);
const commentList = (params)=>getAction("/base/fmCommentInfo/list",params);

//点赞
const isFmLikesDetails = (params)=>getAction("/base/fmLikesDetails/list",params);
const addFmLikesDetails = (params)=>postAction("/base/fmLikesDetails/add",params);

//报表
const fileTypeRadio = (params)=>getAction("/biReport/fileTypeRadio",params);
const fileInOut = (params)=>getAction("/biReport/fileInOut",params);



export {
  fileDetailsInfoGetByPid,
  getFileDetailsInfoList,
  getFileDetailsList,
  dtgDemandInfoAdd,
  dtgTypeDetailsAdd,
  dtgTypeDetailsEdit,
  getFileDetailsInfoByPid,
  fileTransManageAdd,
  addFileDrcInfo,
  isFileDrcDetails,
  dtgEquipmentInfoEdit,
  unzipExportzip,
  fileDetailsInfoList,
  fileTypeRadio,
  fileInOut,
  fileTransManageDelete,
  fileTransManageEdit,
  setFileDetailsPid,
  commentAdd,
  commentList,
  isFmLikesDetails,
  addFmLikesDetails,
}



