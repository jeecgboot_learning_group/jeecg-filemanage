package org.jeecg.modules.filemanage.base.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;


@Data
public class DMFileList implements Serializable {
    /**主键*/
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
    /**创建日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
    /**更新日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
    /**密级*/
    @Excel(name = "密级", width = 15)
    @ApiModelProperty(value = "密级")
    private Integer secretLev;
    /**编号*/
    @Excel(name = "编号", width = 15)
    @ApiModelProperty(value = "编号")
    private String fileCode;
    /**文件名*/
    @Excel(name = "文件名", width = 15)
    @ApiModelProperty(value = "文件名")
    private String fileTitle;
    /**编制单位*/
    @Excel(name = "编制单位", width = 15)
    @ApiModelProperty(value = "编制单位")
    private String orgBelong;
    /**文件描述*/
    @Excel(name = "文件描述", width = 15)
    @ApiModelProperty(value = "文件描述")
    private String fileDescription;
    private String boatBelong;
    /**所属系统*/
    @Excel(name = "所属系统", width = 15)
    @ApiModelProperty(value = "所属系统")
    private String sysBelong;
    /**所属装备*/
    @Excel(name = "所属装备", width = 15)
    @ApiModelProperty(value = "所属装备")
    private String eqpBelong;
    /**文件id*/
    @Excel(name = "文件id", width = 15)
    @ApiModelProperty(value = "文件id")
    private String fileid;
    /**文件路径*/
    @Excel(name = "文件路径", width = 15)
    @ApiModelProperty(value = "文件路径")
    private String filePath;
    /**混淆名*/
    @Excel(name = "混淆名", width = 15)
    @ApiModelProperty(value = "混淆名")
    private String fileMixName;
    /**文档格式*/
    @Excel(name = "文档格式", width = 15)
    @ApiModelProperty(value = "文档格式")
    private String fileTypes;
    /**文档大小(B)*/
    @Excel(name = "文档大小(B)", width = 15)
    @ApiModelProperty(value = "文档大小(B)")
    private Integer fileSize;
    /**上级节点id*/
    @Excel(name = "上级节点id", width = 15)
    @ApiModelProperty(value = "上级节点id")
    private String pid;
    /**版本号*/
    @Excel(name = "版本号", width = 15)
    @ApiModelProperty(value = "版本号")
    private Double versionNo;
    /**主表id*/
    @Excel(name = "版本id", width = 15)
    @ApiModelProperty(value = "版本id")
    private String objid;

}
