package org.jeecg.modules.filemanage.base.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value="BI报表柱状图对象", description="fileInOutView")
public class fileInOutView {
    private String branchid;

    private String branchName;

    private Integer supportNum;

    private Integer requestNum;
}
