package org.jeecg.modules.filemanage.base.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;
import org.jeecg.modules.filemanage.base.mapper.FmFileDrcDetailsMapper;
import org.jeecg.modules.filemanage.base.service.IFmFileDrcDetailsService;
import org.jeecg.modules.filemanage.base.vo.DMFileList;
import org.jeecg.modules.filemanage.base.vo.FileDrcMyList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: fm_file_drc_details
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmFileDrcDetailsServiceImpl extends ServiceImpl<FmFileDrcDetailsMapper, FmFileDrcDetails> implements IFmFileDrcDetailsService {
    @Autowired
    FmFileDrcDetailsMapper fileDrcDetailsMapper;

    public List<DMFileList> getByPid(String pid, String userid, String fileTitle, String fileCode, Integer secretLev, String fileTypes){
        List<DMFileList> list = new ArrayList<DMFileList>();
        list = super.baseMapper.getByPid(pid,userid, fileTitle, fileCode, secretLev, fileTypes);
        return list;
    }

    public Page<FileDrcMyList> mylist(Page<FileDrcMyList> pageList, String createBy, Integer drcType){

        return   pageList.setRecords(fileDrcDetailsMapper.mylist(pageList,createBy,drcType));
    }
}
