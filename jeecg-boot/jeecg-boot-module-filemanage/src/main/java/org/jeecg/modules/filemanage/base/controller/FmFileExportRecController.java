package org.jeecg.modules.filemanage.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.filemanage.base.entity.FmFileExportRec;
import org.jeecg.modules.filemanage.base.service.IFmFileExportRecService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: fm_file_export_rec
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Api(tags="fm_file_export_rec")
@RestController
@RequestMapping("/base/fmFileExportRec")
@Slf4j
public class FmFileExportRecController extends JeecgController<FmFileExportRec, IFmFileExportRecService> {
	@Autowired
	private IFmFileExportRecService fmFileExportRecService;
	
	/**
	 * 分页列表查询
	 *
	 * @param fmFileExportRec
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "fm_file_export_rec-分页列表查询")
	@ApiOperation(value="fm_file_export_rec-分页列表查询", notes="fm_file_export_rec-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FmFileExportRec fmFileExportRec,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FmFileExportRec> queryWrapper = QueryGenerator.initQueryWrapper(fmFileExportRec, req.getParameterMap());
		Page<FmFileExportRec> page = new Page<FmFileExportRec>(pageNo, pageSize);
		IPage<FmFileExportRec> pageList = fmFileExportRecService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param fmFileExportRec
	 * @return
	 */
	@AutoLog(value = "fm_file_export_rec-添加")
	@ApiOperation(value="fm_file_export_rec-添加", notes="fm_file_export_rec-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FmFileExportRec fmFileExportRec) {
		fmFileExportRecService.save(fmFileExportRec);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param fmFileExportRec
	 * @return
	 */
	@AutoLog(value = "fm_file_export_rec-编辑")
	@ApiOperation(value="fm_file_export_rec-编辑", notes="fm_file_export_rec-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FmFileExportRec fmFileExportRec) {
		fmFileExportRecService.updateById(fmFileExportRec);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_export_rec-通过id删除")
	@ApiOperation(value="fm_file_export_rec-通过id删除", notes="fm_file_export_rec-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		fmFileExportRecService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "fm_file_export_rec-批量删除")
	@ApiOperation(value="fm_file_export_rec-批量删除", notes="fm_file_export_rec-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.fmFileExportRecService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "fm_file_export_rec-通过id查询")
	@ApiOperation(value="fm_file_export_rec-通过id查询", notes="fm_file_export_rec-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FmFileExportRec fmFileExportRec = fmFileExportRecService.getById(id);
		if(fmFileExportRec==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(fmFileExportRec);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param fmFileExportRec
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FmFileExportRec fmFileExportRec) {
        return super.exportXls(request, fmFileExportRec, FmFileExportRec.class, "fm_file_export_rec");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FmFileExportRec.class);
    }

}
