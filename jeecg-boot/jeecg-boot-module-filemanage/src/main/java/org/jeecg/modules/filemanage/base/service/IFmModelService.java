package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 模式菜单表
 * @Author: jeecg-boot
 * @Date:   2021-06-09
 * @Version: V1.0
 */
public interface IFmModelService extends IService<FmModel> {

}
