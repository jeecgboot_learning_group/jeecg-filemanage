package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmFractionInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.filemanage.base.vo.commentView;

import java.util.List;

/**
 * @Description: fm_fraction_info
 * @Author: jeecg-boot
 * @Date:   2021-06-21
 * @Version: V1.0
 */
public interface IFmFractionInfoService extends IService<FmFractionInfo> {
    List<commentView> sumfraction(String fileid);
}
