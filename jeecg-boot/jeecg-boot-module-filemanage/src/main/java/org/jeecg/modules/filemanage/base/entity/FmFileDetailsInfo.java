package org.jeecg.modules.filemanage.base.entity;

import java.io.Serializable;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: fm_file_details_info
 * @Author: jeecg-boot
 * @Date:   2021-06-03
 * @Version: V1.0
 */
@Data
@TableName("fm_file_details_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="fm_file_details_info对象", description="fm_file_details_info")
public class FmFileDetailsInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**密级*/
	@Excel(name = "密级", width = 15)
    @ApiModelProperty(value = "密级")
    private Integer secretLev;
	/**编号*/
	@Excel(name = "编号", width = 15)
    @ApiModelProperty(value = "编号")
    private String fileCode;
	/**文件名*/
	@Excel(name = "文件名", width = 15)
    @ApiModelProperty(value = "文件名")
    private String fileTitle;
	/**编制单位*/
	@Excel(name = "编制单位", width = 15)
    @ApiModelProperty(value = "编制单位")
    private String orgBelong;
	/**文件描述*/
	@Excel(name = "文件描述", width = 15)
    @ApiModelProperty(value = "文件描述")
    private String fileDescription;
	/**所属舰船冗余*/
	@Excel(name = "所属舰船冗余", width = 15)
    @ApiModelProperty(value = "所属舰船冗余")
    private String boatBelong;
	/**所属系统*/
	@Excel(name = "所属系统", width = 15)
    @ApiModelProperty(value = "所属系统")
    private String sysBelong;
	/**所属装备*/
	@Excel(name = "所属装备", width = 15)
    @ApiModelProperty(value = "所属装备")
    private String eqpBelong;
	/**文件id*/
	@Excel(name = "文件id", width = 15)
    @ApiModelProperty(value = "文件id")
    private String fileid;
	/**文件路径*/
	@Excel(name = "文件路径", width = 15)
    @ApiModelProperty(value = "文件路径")
    private String filePath;
	/**文档大小(B)*/
	@Excel(name = "文档大小(B)", width = 15)
    @ApiModelProperty(value = "文档大小(B)")
    private Integer fileSize;
	/**文档格式*/
	@Excel(name = "文档格式", width = 15)
    @ApiModelProperty(value = "文档格式")
    private String fileTypes;
	/**混淆名*/
	@Excel(name = "混淆名", width = 15)
    @ApiModelProperty(value = "混淆名")
    private String fileMixName;
	/**上级节点id*/
	@Excel(name = "上级节点id", width = 15)
    @ApiModelProperty(value = "上级节点id")
    private String pid;
	/**模块名*/
	@Excel(name = "模块名", width = 15)
    @ApiModelProperty(value = "模块名")
    private String modelName;
	/**船舷号*/
	@Excel(name = "船舷号", width = 15)
    @ApiModelProperty(value = "船舷号")
    private String sideNum;
	/**所属装备uuid*/
	@Excel(name = "所属装备uuid", width = 15)
    @ApiModelProperty(value = "所属装备uuid")
    private String eqpUid;
	/**所属系统编号*/
	@Excel(name = "所属系统编号", width = 15)
    @ApiModelProperty(value = "所属系统编号")
    private String systemNo;
	/**排序字段*/
	@Excel(name = "排序字段", width = 15)
    @ApiModelProperty(value = "排序字段")
    private Integer idx;

    /** 评分 */
    @TableField(exist = false)
    private java.lang.Double score;
}
