package org.jeecg.modules.filemanage.base.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value="文件搜索结果", description="fileSearchView")
public class fileSearchView {
    //文件id
    private String id;

    private String fileTitle;

    private String fileMixName;

    private String createTime;

    private String fileid;

    //下载次数
    private Integer dlCount;
    //收藏次数
    private Integer colCount;

    //查阅次数
    private Integer viewCount;

    private String createBy;

    private String typeName;

    private Double score;
}
