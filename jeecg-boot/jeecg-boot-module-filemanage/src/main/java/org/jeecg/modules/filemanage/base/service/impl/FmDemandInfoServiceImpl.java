package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmDemandInfo;
import org.jeecg.modules.filemanage.base.mapper.FmDemandInfoMapper;
import org.jeecg.modules.filemanage.base.service.IFmDemandInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: fm_demand_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmDemandInfoServiceImpl extends ServiceImpl<FmDemandInfoMapper, FmDemandInfo> implements IFmDemandInfoService {

}
