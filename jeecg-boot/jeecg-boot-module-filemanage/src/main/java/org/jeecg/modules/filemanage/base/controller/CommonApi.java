package org.jeecg.modules.filemanage.base.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.es.JeecgElasticsearchTemplate;
import org.jeecg.common.util.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 对外开放公用api
 * @Author: chris
 * @Date:   2020-08-29
 * @Version: V1.0
 */
@Api(tags="对外开放公用api")
@RestController
@RequestMapping("/base")
@Slf4j
public class CommonApi {

    @Autowired
    private JeecgElasticsearchTemplate jeecgElasticsearchTemplate;
    /**
     * 获取UUID
     *
     * @param
     * @return
     */
    @AutoLog(value = "所属系统信息-通过id查询")
    @ApiOperation(value="所属系统信息-通过id查询", notes="所属系统信息-通过id查询")
    @GetMapping(value = "/getUUID")
    public Result<?> getUUID() {
       String UUID = UUIDGenerator.generate();
        return Result.ok(UUID);
    }

    @AutoLog(value = "删除指定es数据")
    @ApiOperation(value="删除指定es数据", notes="删除指定es数据")
    @GetMapping(value = "/delES")
    public Result<?> delES(@RequestParam(name="id",required=true) String id,
                           @RequestParam(name="index",required=true) String index,
                           @RequestParam(name="typename",required=true) String typename
    ) {
       boolean res =  jeecgElasticsearchTemplate.delete(index,typename,id);
        return res==true?Result.ok("操作成功"):Result.error("操作失败") ;
    }
}
