package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmFileDetailsHistory;
import org.jeecg.modules.filemanage.base.mapper.FmFileDetailsHistoryMapper;
import org.jeecg.modules.filemanage.base.service.IFmFileDetailsHistoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: fm_file_details_history
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmFileDetailsHistoryServiceImpl extends ServiceImpl<FmFileDetailsHistoryMapper, FmFileDetailsHistory> implements IFmFileDetailsHistoryService {

}
