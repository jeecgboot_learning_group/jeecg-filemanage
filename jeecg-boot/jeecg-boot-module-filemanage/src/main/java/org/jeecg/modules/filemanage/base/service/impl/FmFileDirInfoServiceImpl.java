package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmFileDirInfo;
import org.jeecg.modules.filemanage.base.mapper.FmFileDirInfoMapper;
import org.jeecg.modules.filemanage.base.service.IFmFileDirInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: fm_file_dir_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmFileDirInfoServiceImpl extends ServiceImpl<FmFileDirInfoMapper, FmFileDirInfo> implements IFmFileDirInfoService {
    public List<FmFileDirInfo> getFileDirByFileid(String fileid){
        List<FmFileDirInfo> fdtlist = new ArrayList<FmFileDirInfo>();
        fdtlist = super.baseMapper.getFileDirByFileid(fileid);
        return fdtlist;

    }
}
