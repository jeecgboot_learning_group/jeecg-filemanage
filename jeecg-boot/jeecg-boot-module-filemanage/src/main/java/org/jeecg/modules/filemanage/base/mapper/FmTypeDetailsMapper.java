package org.jeecg.modules.filemanage.base.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmTypeDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.filemanage.base.vo.fileTypesView;

import java.util.List;

/**
 * @Description: fm_type_details
 * @Author: jeecg-boot
 * @Date:   2021-04-14
 * @Version: V1.0
 */
public interface FmTypeDetailsMapper extends BaseMapper<FmTypeDetails> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id,@Param("status") String status);

	List<fileTypesView> queryTypes(Integer depth, Integer num ,String modid);
}
