package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmFileDirInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: fm_file_dir_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface FmFileDirInfoMapper extends BaseMapper<FmFileDirInfo> {
    List<FmFileDirInfo> getFileDirByFileid(String fileid);
}
