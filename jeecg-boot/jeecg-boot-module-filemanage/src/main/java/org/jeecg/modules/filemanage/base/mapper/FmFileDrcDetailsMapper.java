package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.filemanage.base.vo.DMFileList;
import org.jeecg.modules.filemanage.base.vo.FileDrcMyList;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Description: fm_file_drc_details
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface FmFileDrcDetailsMapper extends BaseMapper<FmFileDrcDetails> {
    List<DMFileList>getByPid(String pid, String userid, String fileTitle, String fileCode, Integer secretLev, String fileTypes);

    List<FileDrcMyList> mylist(Page<FileDrcMyList> pageList,String createBy,Integer drcType);
}
