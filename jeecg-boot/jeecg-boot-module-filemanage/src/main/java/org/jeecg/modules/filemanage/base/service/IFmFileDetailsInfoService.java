package org.jeecg.modules.filemanage.base.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.entity.FmFileDetailsInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.filemanage.base.vo.DMFileList;

import java.util.List;

/**
 * @Description: fm_file_details_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmFileDetailsInfoService extends IService<FmFileDetailsInfo> {
    Page<FmFileDetailsInfo> queryPagelistNopid(Page<FmFileDetailsInfo> page, String userId, String msgCategory);

    List<DMFileList> getByPid(String pid, String fileTitle, String fileCode, Integer secretLev, String eqpBelong, Integer type);

    boolean setPid(String ids,String pid);
}
