package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmLikesDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 点赞表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface FmLikesDetailsMapper extends BaseMapper<FmLikesDetails> {

}
