package org.jeecg.modules.filemanage.base.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcDetails;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcInfo;
import org.jeecg.modules.filemanage.base.mapper.FmFileDrcInfoMapper;
import org.jeecg.modules.filemanage.base.service.IFmFileDrcDetailsService;
import org.jeecg.modules.filemanage.base.service.IFmFileDrcInfoService;
import org.jeecg.modules.filemanage.base.vo.DRCByFileid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: fm_file_drc_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmFileDrcInfoServiceImpl extends ServiceImpl<FmFileDrcInfoMapper, FmFileDrcInfo> implements IFmFileDrcInfoService {
    @Autowired
    private  IFmFileDrcInfoService iFileDrcInfoService;
    @Autowired
    private IFmFileDrcDetailsService iFileDrcDetailsService;

    @Transactional
    public boolean addall(Map<String,Object> map) {
        boolean savedir = false, savedts = false;
        FmFileDrcInfo fileDirInfo = JSON.parseObject(JSONObject.toJSONString(map.get("obj")), FmFileDrcInfo.class);
        FmFileDrcDetails fileDetailsInfo = JSON.parseObject(JSONObject.toJSONString(map.get("obb")), FmFileDrcDetails.class);
        List<FmFileDrcInfo> drcInfos = new ArrayList<FmFileDrcInfo>();
        List<FmFileDrcDetails> drcDetails = new ArrayList<FmFileDrcDetails>();
        Map<String,Object> param = new HashMap<>();
        param.put("fileid",fileDirInfo.getFileid());
        drcInfos =  iFileDrcInfoService.listByMap(param);
        if(drcInfos.size()>0) {
            if (fileDetailsInfo.getDrcType() == 0) {//阅读数+1
                drcInfos.get(0).setReadTimes(drcInfos.get(0).getReadTimes() + 1);
            } else if (fileDetailsInfo.getDrcType() == 1) {//下载数+1
                drcInfos.get(0).setDownloadTimes(drcInfos.get(0).getDownloadTimes() + 1);
            } else {//收藏数+1
                param.put("create_by",fileDetailsInfo.getCreateBy());
                param.put("drc_type",2);
                drcDetails = iFileDrcDetailsService.listByMap(param); //已有收藏的 回避重复收藏的逻辑
                drcInfos.get(0).setCollectTimes(drcInfos.get(0).getCollectTimes() + 1);
            }
            savedir = iFileDrcInfoService.updateById(drcInfos.get(0));
        }
        else {
            savedir = iFileDrcInfoService.save(fileDirInfo);
        }

        if(drcDetails.size()<1)
            savedts = iFileDrcDetailsService.save(fileDetailsInfo);

        if(!savedir||!savedts) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        else
            return true;
    }

    public List<DRCByFileid> getByFiled(String fileid, String userid){
        List<DRCByFileid> list = new ArrayList<DRCByFileid>();
        List<FmFileDrcDetails> fileDrcDetails = new ArrayList<FmFileDrcDetails>();
        list = baseMapper.getByFiled(fileid,userid);
        Map<String,Object> param = new HashMap<>();
        param.put("fileid",fileid);
        param.put("create_by",userid);
        fileDrcDetails = iFileDrcDetailsService.listByMap(param);
        if(list.size()>0) {
            if (fileDrcDetails.size() > 0)
                list.get(0).setIsCollected(true);
            else
                list.get(0).setIsCollected(false);
        }
        return list;
    }
}
