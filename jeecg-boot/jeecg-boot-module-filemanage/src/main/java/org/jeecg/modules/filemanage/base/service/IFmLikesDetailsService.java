package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmLikesDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 点赞表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface IFmLikesDetailsService extends IService<FmLikesDetails> {
   boolean addlike(FmLikesDetails fmLikesDetails);
}
