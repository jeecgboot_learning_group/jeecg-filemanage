package org.jeecg.modules.filemanage.base.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.vo.fileInOutView;
import org.jeecg.modules.filemanage.base.vo.fileSearchView;
import org.jeecg.modules.filemanage.base.vo.fileTypeRadioView;

import java.util.List;

public interface BIReportMapper {

    List<fileTypeRadioView> fileTypeRadio(String fileType);

    List<fileInOutView>demandinfo (String branchId);

    List<fileInOutView>exportinfo (String branchId);

    List<fileSearchView>fileSearch(Page<fileSearchView> page,String keyword,String typeName,String suffix,String fileids);

    List<fileSearchView> fileHot(Page<fileSearchView> page, String userId, String msgCategory);

    List<fileSearchView>fileNew(Page<fileSearchView>page,String typeId,String msgCategory);
}
