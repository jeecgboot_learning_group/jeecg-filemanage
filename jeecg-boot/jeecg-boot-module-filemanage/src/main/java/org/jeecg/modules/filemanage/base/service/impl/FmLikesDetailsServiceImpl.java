package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmCommentInfo;
import org.jeecg.modules.filemanage.base.entity.FmLikesDetails;
import org.jeecg.modules.filemanage.base.mapper.FmCommentInfoMapper;
import org.jeecg.modules.filemanage.base.mapper.FmLikesDetailsMapper;
import org.jeecg.modules.filemanage.base.service.IFmLikesDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 点赞表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
@Service
public class FmLikesDetailsServiceImpl extends ServiceImpl<FmLikesDetailsMapper, FmLikesDetails> implements IFmLikesDetailsService {
    @Autowired
    private FmLikesDetailsMapper fmLikesDetailsMapper;
    @Autowired
    private FmCommentInfoMapper fmCommentInfoMapper;

    @Transactional
    public boolean addlike(FmLikesDetails fmLikesDetails){
        boolean res = false;
        int cnt ;
        Map<String,Object> params =new HashMap<>();
        if(fmLikesDetails.getStatus()==0) //取消点赞
        {
            params.put("pid",fmLikesDetails.getPid());
            params.put("create_by",fmLikesDetails.getCreateBy());
            fmLikesDetailsMapper.deleteByMap(params);
         cnt = fmCommentInfoMapper.updateLikeById(fmLikesDetails.getPid(),0);
        }else{
            fmLikesDetailsMapper.insert(fmLikesDetails);
          cnt =  fmCommentInfoMapper.updateLikeById(fmLikesDetails.getPid(),1);
        }

        if(cnt>0)
            res = true;

        return  res;
    }
}
