package org.jeecg.modules.filemanage.base.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.es.JeecgElasticsearchTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class essearchImpl {

    @Autowired
    private JeecgElasticsearchTemplate jet;

    public List<String> queryBykw (String keyword){
        JSONObject res = null;
        JSONArray resmap = null;
        List<String> fileids = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        //搜索文件信息
        sb.append("{")
                .append("\"query\": {")
                .append("\"multi_match\":{")
                .append("\"query\": \"")
                .append(keyword).append("\",")
                .append("\"fields\": [ \"fileTitle\", \"filePath\", \"content\"]")
                .append("}}}");
        JSONObject queryBuilder  =  JSONObject.parseObject(sb.toString());
        res = jet.search("doc_cn_index","filemanage",queryBuilder);
        resmap = res.getJSONObject("hits").getJSONArray("hits");
        if(resmap.size()>0)
        {
            for(int i=0;i<resmap.size();i++) {
                fileids.add(resmap.getJSONObject(i).get("_id").toString());
            }
        }

        //搜索内容
        res = jet.search("filemanage_index","filemanage",queryBuilder);
        resmap = res.getJSONObject("hits").getJSONArray("hits");
        if(resmap.size()>0)
        {
            for(int i=0;i<resmap.size();i++) {
                fileids.add(resmap.getJSONObject(i).get("_id").toString());
            }
        }

        //搜索评论
        res = jet.search("comment_index","filemanage",queryBuilder);
        resmap = res.getJSONObject("hits").getJSONArray("hits");
        if(resmap.size()>0)
        {
            for(int i=0;i<resmap.size();i++) {
                Map<String,String> hs = (Map<String, String>) resmap.getJSONObject(i).get("_source");
                fileids.add(hs.get("fileid"));
            }
        }

        return fileids;

    }
}
