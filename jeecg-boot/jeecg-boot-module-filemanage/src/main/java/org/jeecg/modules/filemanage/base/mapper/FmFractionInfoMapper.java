package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmFractionInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.filemanage.base.vo.commentView;

/**
 * @Description: fm_fraction_info
 * @Author: jeecg-boot
 * @Date:   2021-06-21
 * @Version: V1.0
 */
public interface FmFractionInfoMapper extends BaseMapper<FmFractionInfo> {
    List<commentView> sumFraction(String fileid);
}
