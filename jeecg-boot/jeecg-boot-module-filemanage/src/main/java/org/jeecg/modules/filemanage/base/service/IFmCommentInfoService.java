package org.jeecg.modules.filemanage.base.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.entity.FmCommentInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 评论表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface IFmCommentInfoService extends IService<FmCommentInfo> {
    Page<FmCommentInfo> queryPagelikeList(Page<FmCommentInfo>pageList, String userid,String fileid);

    boolean add (FmCommentInfo fmCommentInfo);

    boolean del (String  id);
}
