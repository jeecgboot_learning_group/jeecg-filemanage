package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmFileDirInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: fm_file_dir_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmFileDirInfoService extends IService<FmFileDirInfo> {
    List<FmFileDirInfo> getFileDirByFileid(String fileid);
}
