package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmDemandDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: fm_demand_details
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmDemandDetailsService extends IService<FmDemandDetails> {

}
