package org.jeecg.modules.filemanage.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.filemanage.base.entity.FmFileDrcInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.filemanage.base.vo.DRCByFileid;

/**
 * @Description: fm_file_drc_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface FmFileDrcInfoMapper extends BaseMapper<FmFileDrcInfo> {
    List<DRCByFileid> getByFiled(String fileid, String userid);
}
