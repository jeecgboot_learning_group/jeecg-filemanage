package org.jeecg.modules.filemanage.base.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.models.auth.In;
import org.jeecg.modules.filemanage.base.entity.FmFileDetailsInfo;
import org.jeecg.modules.filemanage.base.mapper.FmFileDetailsInfoMapper;
import org.jeecg.modules.filemanage.base.service.IFmFileDetailsInfoService;
import org.jeecg.modules.filemanage.base.vo.DMFileList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: fm_file_details_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
@Service
public class FmFileDetailsInfoServiceImpl extends ServiceImpl<FmFileDetailsInfoMapper, FmFileDetailsInfo> implements IFmFileDetailsInfoService {

    @Autowired
    private FmFileDetailsInfoMapper fmFileDetailsInfoMapper;

    //根据上级节点id 获取文件列表信息
    public List<DMFileList> getByPid(String pid, String fileTitle, String fileCode, Integer secretLev, String eqpBelong, Integer type){
        List<DMFileList> list = new ArrayList<DMFileList>();
        if(type ==null)
            type =0;
        if(type==1)//节点是设备而不是目录  将pid转化为原始设备的id寻找文件关联
        {

        }
        list =  super.baseMapper.getByPid(pid, fileTitle, fileCode, secretLev, eqpBelong);
        //特殊节点求证
        return list;
    }
    @Override
    public Page<FmFileDetailsInfo> queryPagelistNopid(Page<FmFileDetailsInfo> page, String userId, String msgCategory) {
        return page.setRecords(fmFileDetailsInfoMapper.queryPagelistNopid(page, userId, msgCategory));
    }

    public boolean setPid(String ids,String pid){
        boolean res = true;
        String whereSql ="";
        Integer count = 1;
        for(String i : ids.split(",")){
            whereSql = "update fm_file_details_info set pid='"+pid +"' where id = '"+i +"';";
            count = fmFileDetailsInfoMapper.setPid(whereSql);
            if(count<0) {
                res = false;
                break;
            }
        }
        //params.put("wherSql",whereSql);


        return res;
    }
}
