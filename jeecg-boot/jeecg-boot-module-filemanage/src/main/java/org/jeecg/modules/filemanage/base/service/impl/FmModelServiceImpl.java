package org.jeecg.modules.filemanage.base.service.impl;

import org.jeecg.modules.filemanage.base.entity.FmModel;
import org.jeecg.modules.filemanage.base.mapper.FmModelMapper;
import org.jeecg.modules.filemanage.base.service.IFmModelService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 模式菜单表
 * @Author: jeecg-boot
 * @Date:   2021-06-09
 * @Version: V1.0
 */
@Service
public class FmModelServiceImpl extends ServiceImpl<FmModelMapper, FmModel> implements IFmModelService {

}
