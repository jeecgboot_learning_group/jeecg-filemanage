package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmDemandInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: fm_demand_info
 * @Author: jeecg-boot
 * @Date:   2021-04-13
 * @Version: V1.0
 */
public interface IFmDemandInfoService extends IService<FmDemandInfo> {

}
