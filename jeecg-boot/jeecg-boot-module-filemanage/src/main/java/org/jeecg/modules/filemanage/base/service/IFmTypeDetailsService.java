package org.jeecg.modules.filemanage.base.service;

import org.jeecg.modules.filemanage.base.entity.FmTypeDetails;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.filemanage.base.vo.fileTypesView;

import java.util.List;

/**
 * @Description: fm_type_details
 * @Author: jeecg-boot
 * @Date:   2021-04-14
 * @Version: V1.0
 */
public interface IFmTypeDetailsService extends IService<FmTypeDetails> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";

	/**树节点有子节点状态值*/
	public static final String HASCHILD = "1";

	/**树节点无子节点状态值*/
	public static final String NOCHILD = "0";

	/**新增节点*/
	void addFmTypeDetails(FmTypeDetails fmTypeDetails);

	/**修改节点*/
	void updateFmTypeDetails(FmTypeDetails fmTypeDetails) throws JeecgBootException;

	/**删除节点*/
	void deleteFmTypeDetails(String id) throws JeecgBootException;

	List<fileTypesView> queryTypes(int depth, int num ,String modid);
}
