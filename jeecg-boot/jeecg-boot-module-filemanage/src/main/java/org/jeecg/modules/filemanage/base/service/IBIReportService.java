package org.jeecg.modules.filemanage.base.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.filemanage.base.vo.fileInOutView;
import org.jeecg.modules.filemanage.base.vo.fileSearchView;
import org.jeecg.modules.filemanage.base.vo.fileTypeRadioView;

import java.util.List;

public interface IBIReportService {
    List<fileTypeRadioView> fileTypeRadio(String fileType);

    List<fileInOutView> fileInOut(String branchId);

    Page<fileSearchView>fileSearch (Page<fileSearchView> page,String keyword,String typeName,String suffix);

    Page<fileSearchView> fileHot(Page<fileSearchView> page, String userId, String msgCategory);

    fileInOutView totalDoc();

    Page<fileSearchView>fileNew(Page<fileSearchView> page, String typeId, String msgCategory);

    fileInOutView userFileInfo(String userid);

    List<String> essearch(String userid);
}
