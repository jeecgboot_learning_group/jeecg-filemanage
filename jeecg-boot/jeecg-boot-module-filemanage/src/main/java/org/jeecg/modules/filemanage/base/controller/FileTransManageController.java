package org.jeecg.modules.filemanage.base.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.filemanage.base.service.IFileTransManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description: file_trans_manage
 * @Author: chris
 * @Date:   2020-08-21
 * @Version: V1.0
 */
@Api(tags="file_trans_manage")
@RestController
@RequestMapping("/filemanage/fileTransManage")
@Slf4j
public class FileTransManageController {
    @Autowired
    private IFileTransManageService iFileTransManageService;

    @AutoLog(value = "技术资料文件添加")
    @ApiOperation(value="技术资料文件添加", notes="技术资料文件添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody Map<String,Object> map) {

        boolean res = iFileTransManageService.add(map);
        return res == true ?  Result.ok("添加成功！"): Result.error("添加失败！");
    }

    @AutoLog(value = "技术资料文件编辑")
    @ApiOperation(value="技术资料文件编辑", notes="技术资料文件编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody Map<String,Object> map) {

        boolean res = iFileTransManageService.edit(map);
        return res == true ?  Result.ok("编辑成功！"): Result.error("编辑失败！");
    }

    @AutoLog(value = "技术资料文件删除")
    @ApiOperation(value="技术资料文件删除", notes="技术资料文件删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delMod(@RequestParam(name="ids",required=true) String ids) {

        boolean res =iFileTransManageService.delete(ids);
        return res == true ?  Result.ok("删除成功！"): Result.error("删除失败！");
    }




}
