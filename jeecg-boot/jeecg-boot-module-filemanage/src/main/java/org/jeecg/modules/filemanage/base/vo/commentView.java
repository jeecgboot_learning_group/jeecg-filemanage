package org.jeecg.modules.filemanage.base.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class commentView implements Serializable {
    private String id;

    private String fileid;

    private String content;

    private Double score;
}
