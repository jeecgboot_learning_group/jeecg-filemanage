package org.jeecg.modules.video.base.service;

import org.jeecg.modules.video.base.entity.VideoFileDir;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: video_file_dir
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
public interface IVideoFileDirService extends IService<VideoFileDir> {

}
