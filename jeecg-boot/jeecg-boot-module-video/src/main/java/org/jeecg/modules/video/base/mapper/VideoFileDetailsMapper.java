package org.jeecg.modules.video.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.video.base.entity.VideoFileDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: video_file_details
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
public interface VideoFileDetailsMapper extends BaseMapper<VideoFileDetails> {

}
