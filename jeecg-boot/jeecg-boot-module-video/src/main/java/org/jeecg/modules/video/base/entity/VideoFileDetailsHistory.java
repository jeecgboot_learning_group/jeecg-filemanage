package org.jeecg.modules.video.base.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: video_file_details_history
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
@Data
@TableName("video_file_details_history")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="video_file_details_history对象", description="video_file_details_history")
public class VideoFileDetailsHistory implements Serializable {
    private static final long serialVersionUID = 1L;

	/**编号*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "编号")
    private java.lang.Integer id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**文件名*/
	@Excel(name = "文件名", width = 15)
    @ApiModelProperty(value = "文件名")
    private java.lang.String fileTitle;
	/**文件描述*/
	@Excel(name = "文件描述", width = 15)
    @ApiModelProperty(value = "文件描述")
    private java.lang.String fileDescription;
	/**所属系统*/
	@Excel(name = "所属系统", width = 15)
    @ApiModelProperty(value = "所属系统")
    private java.lang.String sysBelong;
	/**文件id*/
	@Excel(name = "文件id", width = 15)
    @ApiModelProperty(value = "文件id")
    private java.lang.String fileid;
	/**文件路径*/
	@Excel(name = "文件路径", width = 15)
    @ApiModelProperty(value = "文件路径")
    private java.lang.String filePath;
	/**文档大小(B)*/
	@Excel(name = "文档大小(B)", width = 15)
    @ApiModelProperty(value = "文档大小(B)")
    private java.lang.Integer fileSize;
	/**文档格式*/
	@Excel(name = "文档格式", width = 15)
    @ApiModelProperty(value = "文档格式")
    private java.lang.String fileTypes;
	/**混淆名*/
	@Excel(name = "混淆名", width = 15)
    @ApiModelProperty(value = "混淆名")
    private java.lang.String fileMixName;
	/**封面路径*/
	@Excel(name = "封面路径", width = 15)
    @ApiModelProperty(value = "封面路径")
    private java.lang.String fileCover;
	/**pid*/
	@Excel(name = "pid", width = 15)
    @ApiModelProperty(value = "pid")
    private java.lang.String pid;
	/**模块名*/
	@Excel(name = "模块名", width = 15)
    @ApiModelProperty(value = "模块名")
    private java.lang.String modelName;
	/**操作类型*/
	@Excel(name = "操作类型", width = 15)
    @ApiModelProperty(value = "操作类型")
    private java.lang.Integer opt;
	/**当时版本*/
	@Excel(name = "当时版本", width = 15)
    @ApiModelProperty(value = "当时版本")
    private java.lang.Double versionNo;
}
