package org.jeecg.modules.video.base.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.video.base.entity.VideoTypeDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: video_type_details
 * @Author: jeecg-boot
 * @Date:   2021-06-28
 * @Version: V1.0
 */
public interface VideoTypeDetailsMapper extends BaseMapper<VideoTypeDetails> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id, @Param("status") String status);

}
