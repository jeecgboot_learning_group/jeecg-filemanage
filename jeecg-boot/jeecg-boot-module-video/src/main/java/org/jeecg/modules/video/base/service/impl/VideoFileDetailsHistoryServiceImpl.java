package org.jeecg.modules.video.base.service.impl;

import org.jeecg.modules.video.base.entity.VideoFileDetailsHistory;
import org.jeecg.modules.video.base.mapper.VideoFileDetailsHistoryMapper;
import org.jeecg.modules.video.base.service.IVideoFileDetailsHistoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: video_file_details_history
 * @Author: jeecg-boot
 * @Date:   2021-06-25
 * @Version: V1.0
 */
@Service
public class VideoFileDetailsHistoryServiceImpl extends ServiceImpl<VideoFileDetailsHistoryMapper, VideoFileDetailsHistory> implements IVideoFileDetailsHistoryService {

}
