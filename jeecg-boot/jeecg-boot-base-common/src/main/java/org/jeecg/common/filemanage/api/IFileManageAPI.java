package org.jeecg.common.filemanage.api;

import org.springframework.web.multipart.MultipartFile;

public interface IFileManageAPI {
    void readFileStream(MultipartFile mf, String fileId);
}
